#[macro_use]
extern crate serde_derive;
extern crate serde_json;

extern crate num_cpus;

#[derive(Debug, Deserialize)]
#[serde(rename_all = "PascalCase")]
struct Config {
    over_ride: usize,
}

mod lib;

fn main() {
    let _data = lib::LoadGenHandler.getConfigFile();
    let config = parseConfig(_data.to_string());
    let _threads = lib::LoadGenHandler.getMaxThreads();
    if config.over_ride > 0 {
        lib::LoadGenHandler.generateCpuLoad(config.over_ride);
    } else {
        lib::LoadGenHandler.generateCpuLoad(_threads);
    }
}

fn parseConfig(_data: String) -> Config {
    let config: Config = serde_json::from_str(&_data)
        .expect("JSON Config was not formatted correctly!");
    return config;
}
