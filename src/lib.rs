use std::fs;
use std::thread;
use primes::PrimeSet;
use std::process::exit;
use threadpool::ThreadPool;
use std::sync::mpsc::channel;

extern crate num_cpus;
#[derive(Debug)]
pub struct LoadGenHandler;
pub struct Threads {
    num_workers: usize,
    num_jobs: usize,
}

impl LoadGenHandler {
    pub fn getConfigFile(self) -> String {
        let _data = fs::read_to_string("./config.json")
                .expect("Unable to read configuration file");
        return _data;
    }
    pub fn getMaxThreads(self) -> usize {
        return num_cpus::get();
    }
    pub fn generateCpuLoad(self, nthreads: usize) {
        let mut threadpool = Threads { num_workers: 0, num_jobs:
            self.getMaxThreads() };
        threadpool.num_workers = threadpool.num_jobs;
        let pool = ThreadPool::new(threadpool.num_workers);
        let (tx, rx) = channel();
        for _ in 0..threadpool.num_jobs {
            let tx = tx.clone();
            pool.execute(move|| {
                let mut _pset = PrimeSet::new();
                let (_ix, _n) = _pset.find(3_000_000_000);
                tx.send(1);
            });
        }
        rx.iter().take(threadpool.num_jobs).fold(0, |a,b|a+b);
    }
}
