#### This is intended for generating heavy load on machines

## TODO
- Implement GPU crunching
- Prepare for Mac/Windows environments
- ~~Implement ThreadPools~~
- ~~Implement a config file for max # of threads~~
- ~~Add CI/CD~~
- ~~Move to abstracted and object oriented~~

## How to
The configuration file accepts an integer in OverRide. This is meant to be any value between 0 and MAX_THREADS. If the value is 0 the program will default to max CPU usage.
